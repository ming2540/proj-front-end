import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { PoemService } from '../services/poem.service';
import { element } from 'protractor';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  currentUser;
  showPoems: boolean = true;
  showComments: boolean = false;
  myPoemList = [];
  myCommentList = [];

  constructor(private auth: AuthService, private poemService: PoemService) { }

  ngOnInit() { }

  getCurrentUser() {
    this.currentUser = this.auth.getCurrentUser()
    return this.currentUser? true : false;
  }

  showPoemSegment() {
    this.showPoems = true;
    this.showComments = false;  
  }

  showCommentSegment() {
    this.showPoems = false;
    this.showComments = true;  
  }

}
