import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WritePoemPageRoutingModule } from './write-poem-routing.module';

import { WritePoemPage } from './write-poem.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WritePoemPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [WritePoemPage]
})
export class WritePoemPageModule {}
