import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WritePoemPage } from './write-poem.page';

describe('WritePoemPage', () => {
  let component: WritePoemPage;
  let fixture: ComponentFixture<WritePoemPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WritePoemPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(WritePoemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
