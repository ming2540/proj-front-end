import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PoemService } from '../services/poem.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  poems = [];
  private subscription: Subscription;

  constructor(private poemService: PoemService) {}

  ngOnInit() {
    this.subscription = this.poemService.getAll().subscribe(
      poems => {
        this.poems = poems.result;
      }
    )
  }

}
