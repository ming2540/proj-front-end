import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AppendPoemPageRoutingModule } from './append-poem-routing.module';

import { AppendPoemPage } from './append-poem.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AppendPoemPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [AppendPoemPage]
})
export class AppendPoemPageModule {}
