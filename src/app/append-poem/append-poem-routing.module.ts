import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppendPoemPage } from './append-poem.page';

const routes: Routes = [
  {
    path: '',
    component: AppendPoemPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppendPoemPageRoutingModule {}
