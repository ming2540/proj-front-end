import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Chart } from 'chart.js';
import { PoemService } from 'src/app/services/poem.service';
import { Circle } from 'progressbar.js';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-poem-info',
  templateUrl: './poem-info.component.html',
  styleUrls: ['./poem-info.component.scss'],
})
export class PoemInfoComponent implements OnInit, AfterViewInit {

  @ViewChild('pieCanvas', {static: false}) pieCanvas;
  @ViewChild('progressBar', {static: false}) progressBar;
  @ViewChild('comment', {static: false}) comment;

  commentContent = "";
  upvoted: boolean = false;
  pieChart: any;
  private subscription: Subscription;
  poem;
  author = [];
  showChart: boolean = false;
  content = [];
  pronounce = []
  key = [];
  successTarget = [];
  dangerTarget = [];
  showRhyme: boolean = false;
  info = {
    perfect: false,
    inter_section_rhymes: false,
    intra_section_rhymes: false,
    alphabet_rhymes: false,
    tones: false,
    inter_chapter_rhymes: false,
  };
  guide = {
    perfect: false,
    inter_section_rhymes: false,
    intra_section_rhymes: false,
    alphabet_rhymes: false,
    tones: false,
    inter_chapter_rhymes: false,
  }
  commentAuthor = []
  suggestWord = [];

  constructor(private poemService: PoemService, private route: ActivatedRoute, private router: Router, private authService: AuthService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      const id = paramMap.get('poem_id');
      this.getPoemDetail(id);
    })
    this.route.queryParams.subscribe(params => {
      Object.keys(params).map(word => {
        this.suggestWord.push({original: word, suggest: params[word]})
      })
    })
  }

  getPoemDetail(id){
    this.subscription = this.poemService.get(id).subscribe(poem => {
      this.poem = poem;
      this.author = [];
      this.content = [];
      for(let author of this.poem.author) {
        this.author.push(this.poemService.getAuthor(author['$id']['$oid']));
      }
      this.poem.result.content.map(el => {
        this.content.push(el.join(''));
      })
      this.poem.result.pronounce.map(el => {
        this.pronounce.push(el.split('-'));
        let k = [];
        for(let i = 0; i<el.split('-').length; i++){
          k.push('');
        }
        this.key.push(k);
      })
      for(let comment of this.poem.comment) {
        this.commentAuthor.push(this.poemService.getAuthor(comment['author']['$id']['$oid']));
      }
      console.log(this.poem)
    })
  }

  ngAfterViewInit() {
    const self = this
    setTimeout(() => {
      self.renderProgressBar();
      self.renderPieChart();
    }, 1000);
  }

  renderProgressBar(){
    let progressBar = new Circle('#progressBar', {
      color: '#aaa',
      // This has to be the same size as the maximum width to
      // prevent clippingz
      strokeWidth: 4,
      trailWidth: 4,
      easing: 'easeInOut',
      duration: 1400,
      text: {
        autoStyleContainer: false
      },
      from: { color: '#aaa', width: 4 },
      to: { color: '#FF5555', width: 4 },
      // Set default step function for all animate calls
      step: function(state, circle) {
        circle.path.setAttribute('stroke', state.color);
        circle.path.setAttribute('stroke-width', state.width);
    
        var value = Math.round(circle.value() * 100);
        if (value === 0) {
          circle.setText('');
        } else {
          circle.setText(value + '/100 คะแนน');
        }
      }
    });
    progressBar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
    progressBar.text.style.fontSize = '3rem';

    progressBar.animate(this.poem.result.score/100);  // Number from 0.0 to 1.0
  }

  renderPieChart() {
    if(!this.pieCanvas || !this.pieCanvas.nativeElement || !this.poem.result) {
      return 
    } else if(this.poem.poem_type == 'กลอนแปดสุภาพ') {
      this.pieChart = new Chart(this.pieCanvas.nativeElement, {
        type: 'pie',
        data: {
          labels: ['สัมผัสนอก', 'สัมผัสใน', 'สัมผัสระหว่างบท', 'สัมผัสพยัญชนะ', 'วรรณยุกต์ลงท้าย'],
          datasets: [{
            label: 'คะแนน',
            data: [
              this.poem.result.inter_section_rhymes.reduce((prev, cur) => prev+cur*15, 0),
              this.poem.result.intra_section_rhymes.reduce((prev, cur) => prev+cur*3, 0),
              this.poem.result.inter_chapter_rhymes.reduce((prev, cur) => prev+cur*5, 0),
              this.poem.result.alphabet_rhymes.reduce((prev, cur) => prev+cur, 0),
              this.poem.result.tones.reduce((prev, cur) => prev+cur*3, 0),
            ],
            backgroundColor: [
              'rgb(255, 99, 132)',
              'rgb(54, 162, 235)',
              'rgb(255, 205, 86)',
              'rgb(75, 192, 192)',
              'rgb(255, 159, 64)',
            ],
          }]
        },
      });
    } else if(this.poem.poem_type == 'โคลงสี่สุภาพ'){
      this.pieChart = new Chart(this.pieCanvas.nativeElement, {
        type: 'pie',
        data: {
          labels: ['สัมผัสนอก', 'สัมผัสใน', 'สัมผัสระหว่างบท', 'สัมผัสพยัญชนะ', 'วรรณยุกต์บังคับ', 'อื่นๆ'],
          datasets: [{
            label: 'คะแนน',
            data: [
              this.poem.result.inter_section_rhymes.reduce((prev, cur) => prev+cur*15, 0),
              this.poem.result.intra_section_rhymes.reduce((prev, cur) => prev+cur*3, 0),
              this.poem.result.inter_chapter_rhymes.reduce((prev, cur) => prev+cur*5, 0),
              this.poem.result.alphabet_rhymes.reduce((prev, cur) => prev+cur, 0),
              this.poem.result.tones.reduce((prev, cur) => prev+cur*3, 0),
              this.poem.result.score - (this.poem.result.inter_section_rhymes.reduce((prev, cur) => prev+cur*15, 0) +
                this.poem.result.intra_section_rhymes.reduce((prev, cur) => prev+cur*3, 0) +
                this.poem.result.inter_chapter_rhymes.reduce((prev, cur) => prev+cur*5, 0) +
                this.poem.result.alphabet_rhymes.reduce((prev, cur) => prev+cur, 0) +
                this.poem.result.tones.reduce((prev, cur) => prev+cur*3, 0)),
            ],
            backgroundColor: [
              'rgb(255, 99, 132)',
              'rgb(54, 162, 235)',
              'rgb(255, 205, 86)',
              'rgb(75, 192, 192)',
              'rgb(255, 159, 64)',
              'rgb(26, 159, 64)',
            ],
          }]
        },
      });
    }
  }

  toggleExpandChart() {
    this.showChart = !this.showChart;
  }

  appendPoem(id){
    this.router.navigateByUrl('tabs/append_poem/' + id);
  }

  toggleCheckRhyme(type, i){
    if(this.showRhyme){
      this.resetCheckRhyme();
    } else {
      this.checkRhymes(type, i);
    }
    this.showRhyme = ! this.showRhyme;
  }

  resetCheckRhyme(){
    this.key = [];
    this.poem.result.pronounce.map(el => {
      let k = [];
      for(let i = 0; i<el.split('-').length; i++){
        k.push('');
      }
      this.key.push(k);
    });
  }

  checkRhymes(type, i){
    this.resetCheckRhyme()
    let poemSize = 3;
    if(this.poem.poem_type == 'กลอนแปดสุภาพ') {
      poemSize = 3;
    } else {
      poemSize = 8;
    }
    const index = parseInt(i+(i/poemSize));
    switch(type) {
      case 0: switch(i%3){
                case 0: if(this.poem.result.inter_section_rhymes[i] == 1) {
                          this.key[index][this.key[index].length-1] = "success";
                          this.key[index+1][2] = "success";
                        } else {
                          this.key[index][this.key[index].length-1] = "danger";
                          this.key[index+1][2] = "danger";
                        }
                        break;
                case 1: if(this.poem.result.inter_section_rhymes[i] == 1){
                          this.key[index][this.key[index].length-1] = "success";
                          this.key[index+1][this.key[index+1].length-1] = "success";
                        } else {
                          this.key[index][this.key[index].length-1] = "danger";
                          this.key[index+1][this.key[index].length-1] = "danger";
                        }
                        break;
                case 2: if(this.poem.result.inter_section_rhymes[i] == 1) {
                          this.key[index][this.key[index].length-1] = "success";
                          this.key[index+1][2] = "success";
                        } else {
                          this.key[index][this.key[index].length-1] = "danger";
                          this.key[index+1][2] = "danger";
                        }
              }
              break;
      case 1: switch(i){
          
              }
              break;
      case 2: switch(i){

              }
              break;
      case 3: if(this.poem.result.tones[i] == 2) {
                this.key[i][this.key[i].length-1] = "success";
              } else if(this.poem.result.tones[i] == 1) {
                this.key[i][this.key[i].length-1] = "secondary";
              } else {
                this.key[i][this.key[i].length-1] = "danger";
              }
              break;
      case 4: switch(i%3){
                case 0: if(this.poem.result.inter_section_rhymes[i] == 1) {
                          this.key[index+1][1] = "success";
                          this.key[index+2][this.key[index+2].length-1] = "success";
                        } else {
                          this.key[index+1][1] = "danger";
                          this.key[index+2][this.key[index+2].length-1] = "danger";
                        }
                        break;
                case 1: if(this.poem.result.inter_section_rhymes[i] == 1){
                          this.key[index][1] = "success";
                          this.key[index+3][this.key[index+3].length-1] = "success";
                        } else {
                          this.key[index][1] = "danger";
                          this.key[index+3][this.key[index+3].length-1]= "danger";
                        }
                        break;
                case 2: if(this.poem.result.inter_section_rhymes[i] == 1) {
                          this.key[index+1][this.key[index+1].length-1] = "success";
                          this.key[index+4][this.key[index+4].length-1] = "success";
                        } else {
                          this.key[index+1][this.key[index+1].length-1] = "danger";
                          this.key[index+4][this.key[index+4].length-1] = "danger";
                        }
              }
              break;
      case 5: switch(i%11){
                case 0: if(this.poem.result.tones[i] == 1) {
                          this.key[index][this.key[index].length-2] = "success";
                        } else {
                          this.key[index][this.key[index].length-2] = "danger";
                        }
                        break;
                case 1: if(this.poem.result.tones[i] == 1) {
                          this.key[index-1][this.key[index+1].length-1] = "success";
                        } else {
                          this.key[index-1][this.key[index+1].length-1] = "danger";
                        }
                        break;
                case 2: if(this.poem.result.tones[i] == 1) {
                          this.key[index][1] = "success";
                        } else {
                          this.key[index][1] = "danger";
                        }
                        break;
                case 3: if(this.poem.result.tones[i] == 1) {
                          this.key[index][0] = "success";
                        } else {
                          this.key[index][0] = "danger";
                        }
                        break;
                case 4: if(this.poem.result.tones[i] == 1) {
                          this.key[index-1][1] = "success";
                        } else {
                          this.key[index-1][1] = "danger";
                        }
                        break;
                case 5: if(this.poem.result.tones[i] == 1) {
                          this.key[index-1][2] = "success";
                        } else {
                          this.key[index-1][2] = "danger";
                        }
                        break;
                case 6: if(this.poem.result.tones[i] == 1) {
                          this.key[index-1][1] = "success";
                        } else {
                          this.key[index-1][1] = "danger";
                        }
                        break;
                case 7: if(this.poem.result.tones[i] == 1) {
                          this.key[index-1][1] = "success";
                        } else {
                          this.key[index-1][1] = "danger";
                        }
                        break;
                case 8: if(this.poem.result.tones[i] == 1) {
                          this.key[index-3][this.key[index-3].length-1] = "success";
                        } else {
                          this.key[index-3][this.key[index-3].length-1] = "danger";
                        }
                        break;
                case 9: if(this.poem.result.tones[i] == 1) {
                          this.key[index-3][0] = "success";
                        } else {
                          this.key[index-3][0] = "danger";
                        }
                        break;
                case 10: if(this.poem.result.tones[i] == 1) {
                          this.key[index-4][1] = "success";
                        } else {
                          this.key[index-4][1] = "danger";
                        }
                        break;
              }
    }
  }

  toggleExpandInfo(info) {
    this.info[info] = !this.info[info];
  }

  toggleExpandGuide(guide) {
    this.guide[guide] = !this.guide[guide];
  }

  onComment(value) {
    console.log(value)
    const content = {
      content: value,
      author: this.authService.getCurrentUser().id,
    }
    this.poemService.comment(content, this.poem._id.$oid).subscribe(
      result => {
        if(this.comment)
          this.comment.value = '';
        this.getPoemDetail(result['id']);
      }
    )
  }

  checkIfUpvoted(){
    if(!this.authService.getCurrentUser()) {
      return false;
    }
    const currentUserId = this.authService.getCurrentUser().id;
    return this.poem.upvote.find(element => element.$id.$oid === currentUserId)? true : false
  }

  toggleUpvote() {
    if(this.checkIfUpvoted()) {
      this.poemService.unvote(this.authService.getCurrentUser().id, this.poem._id.$oid).subscribe(
        result => {
          this.getPoemDetail(result['id']);
        }
      )
    } else {
      this.poemService.upvote(this.authService.getCurrentUser().id, this.poem._id.$oid).subscribe(
        result => {
          this.getPoemDetail(result['id']);
        }
      )
    }
  }

}
