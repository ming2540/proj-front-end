import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { PoemService } from 'src/app/services/poem.service';

@Component({
  selector: 'app-poem',
  templateUrl: './poem.component.html',
  styleUrls: ['./poem.component.scss'],
})
export class PoemComponent implements OnInit {
  
  @Input() poem;
  author = [];
  content = [];
  
  constructor(private router: Router, private poemService: PoemService) { }

  ngOnInit() {
    for(let author of this.poem.author) {
      this.author.push(this.poemService.getAuthor(author['$id']['$oid']));
      this.poem.result.content.map(el => {
        this.content.push(el.join(''));
      })
    }
  }

  goToPoemInfo(id) {
    this.router.navigateByUrl('tabs/poem/'+id);
  }

}
