import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-poems',
  templateUrl: './poems.component.html',
  styleUrls: ['./poems.component.scss'],
})
export class PoemsComponent implements OnInit {

  @Input() poems

  constructor() { }

  ngOnInit() { }

}
