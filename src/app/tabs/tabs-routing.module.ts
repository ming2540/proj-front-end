import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';
import { PoemInfoComponent } from '../components/poem-info/poem-info.component';
import { AuthGuardService } from '../services/auth-guard.service';


const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../home/home.module').then(m => m.HomePageModule)
          }
        ]
      },
      {
        path: 'explore',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../explore/explore.module').then(m => m.ExplorePageModule)
          }
        ]
      },
      {
        path: 'write_poem',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../write-poem/write-poem.module').then(m => m.WritePoemPageModule),
              canActivate: [AuthGuardService],
          }
        ]
      },
      {
        path: 'append_poem/:poem_id',
        children: [
          {
            path: '',
            loadChildren: () => import('../append-poem/append-poem.module').then( m => m.AppendPoemPageModule),
            canActivate: [AuthGuardService],
          }
        ]
      },
      {
        path: 'profile',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../profile/profile.module').then(m => m.ProfilePageModule),
              canActivate: [AuthGuardService],
          }
        ]
      },
      {
        path: 'poem/:poem_id',
        component: PoemInfoComponent
      },
      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
