import { Component, OnInit } from '@angular/core';
import { PoemService } from '../services/poem.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-explore',
  templateUrl: './explore.page.html',
  styleUrls: ['./explore.page.scss'],
})
export class ExplorePage implements OnInit {

  constructor(private poemService: PoemService) { }
  subscription: Subscription;
  page:number = 0;
  poems: any = [];
  next: boolean = true;
  fourPoemList = [];
  eightPoemList = [];
  eightPoem: boolean = true;
  fourPoem: boolean = false;

  ngOnInit() {
    this.subscription = this.poemService.getAll().subscribe(poems => {
      // this.poems = poems.result;
      poems.result.map(element => {
        console.log(element.poem_type)
        if (element.poem_type == 'กลอนแปดสุภาพ') {
          this.eightPoemList.push(element)
        } else {
          this.fourPoemList.push(element)
        }
      });
      this.next = poems.next;
      this.page++;
      console.log(this.eightPoemList, this.fourPoemList)
    })
  }

  changeType(type) {
    console.log(this.eightPoem, this.fourPoem)
    if (type == 1) {
      this.eightPoem = true;
      this.fourPoem = false;
    } else {
      this.eightPoem = false;
      this.fourPoem = true;
    }
  }

  reLoad(event) {
    this.subscription = this.poemService.getAll(this.page).subscribe(poems => {
      // this.poems.push(...poems.result);
      poems.result.map(element => {
        if (element.poem_type == 'กลอนแปดสุภาพ') {
          this.eightPoemList.push(element)
        } else {
          this.fourPoemLsit.push(element)
        }
      })
      this.next = poems.next;
      this.page++;
      event.target.complete();
    })
  }

}
